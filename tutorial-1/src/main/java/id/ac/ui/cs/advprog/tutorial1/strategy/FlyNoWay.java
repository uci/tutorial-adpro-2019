package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {

    public void fly() {
        System.out.println("I CAN'T FLY :(");
    }
}
