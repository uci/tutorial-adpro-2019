package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {

    public SecurityExpert(String nama, double gaji) {
        if (gaji < 70000.00) {
            throw new IllegalArgumentException();
        }
        this.name = nama;
        this.salary = gaji;
        this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
