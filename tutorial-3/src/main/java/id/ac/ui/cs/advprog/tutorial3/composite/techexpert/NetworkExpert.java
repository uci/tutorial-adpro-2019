package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {

    public NetworkExpert(String nama, double gaji) {
        if (gaji < 50000.00) {
            throw new IllegalArgumentException();
        }
        this.name = nama;
        this.salary = gaji;
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
