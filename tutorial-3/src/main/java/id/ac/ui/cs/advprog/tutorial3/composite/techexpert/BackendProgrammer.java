package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {

    public BackendProgrammer(String nama, double gaji) {
        if (gaji < 20000.00) {
            throw new IllegalArgumentException();
        }
        this.name = nama;
        this.salary = gaji;
        this.role =  "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
