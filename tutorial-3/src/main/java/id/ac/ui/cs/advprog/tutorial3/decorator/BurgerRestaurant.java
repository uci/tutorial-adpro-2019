package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class BurgerRestaurant {
    public static void main(String[] args) {
        Food thickBunBurgerSpecial;
        Food thinBunBurgerVegetarian;
        Food doubleBeefChickenDoubleSauceSandwich;
        Food noCrustAllFillingSandwich;

        thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);

        thinBunBurgerVegetarian = BreadProducer.THIN_BUN.createBreadToBeFilled();
        thinBunBurgerVegetarian = FillingDecorator.TOMATO.addFillingToBread(
                thinBunBurgerVegetarian);
        thinBunBurgerVegetarian = FillingDecorator.LETTUCE.addFillingToBread(
                thinBunBurgerVegetarian);
        thinBunBurgerVegetarian = FillingDecorator.CUCUMBER.addFillingToBread(
                thinBunBurgerVegetarian);

        doubleBeefChickenDoubleSauceSandwich =
                BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        doubleBeefChickenDoubleSauceSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        doubleBeefChickenDoubleSauceSandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        doubleBeefChickenDoubleSauceSandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        doubleBeefChickenDoubleSauceSandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);

        noCrustAllFillingSandwich = BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();
        noCrustAllFillingSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.CHEESE.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.CUCUMBER.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.LETTUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.TOMATO.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);

        System.out.println(
                "#1 First burger\n" + noCrustAllFillingSandwich.getDescription() +
                        "\nCost = $" + noCrustAllFillingSandwich.cost());
        System.out.println(
                "#2 Second burger\n" + doubleBeefChickenDoubleSauceSandwich.getDescription() +
                        "\nCost = $" + doubleBeefChickenDoubleSauceSandwich.cost());
        System.out.println(
                "#3 Third burger\n" + thinBunBurgerVegetarian.getDescription() +
                        "\nCost = $" + thickBunBurgerSpecial.cost());
        System.out.println(
                "#4 Fourth burger\n" + thickBunBurgerSpecial.getDescription() +
                        "\nCost = $" + thickBunBurgerSpecial.cost());
    }
}
