package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {

    public FrontendProgrammer(String nama, double gaji) {
        if (gaji < 30000.00) {
            throw new IllegalArgumentException();
        }
        this.name = nama;
        this.salary = gaji;
        this.role =  "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
