package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {

    public UiUxDesigner(String nama, double gaji) {
        if (gaji < 90000.00) {
            throw new IllegalArgumentException();
        }
        this.name = nama;
        this.salary = gaji;
        this.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
